package com.teste.findme.data.db;

import com.teste.findme.data.model.Report;

import java.util.List;

import io.reactivex.Observable;

public interface DbHelper {

    Observable<List<Report>> loadAllReports();

    Observable<Boolean> insertReport(final Report report);
}
