package com.teste.findme.ui.map;

public interface MapNavigation {

    void startReport();

    void openReportActivity();

    void openHistoryActivity();
}
