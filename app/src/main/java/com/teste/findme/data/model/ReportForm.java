package com.teste.findme.data.model;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

public class ReportForm {
    private Report field = new Report();
    private MutableLiveData<Report> buttonClick = new MutableLiveData<>();

    public boolean isValid() {
        boolean valid = isReportValid(false);
        return valid;
    }

    public boolean isReportValid(boolean isValid) {
        // Minimum a@b.c
        String email = field.getReportContent();
        if (email != null && email.length() > 15) {
            return true;
        }
        Log.d("Report:::", "Failure");

        return false;
    }

    public void onClick() {
        if (isValid()) {
            buttonClick.setValue(field);
        }
    }

    public MutableLiveData<Report> getReportField() {
        return buttonClick;
    }

    public Report getFields() {
        return field;
    }

    public void getReportError(){
        Log.d("Report:::", "Failure");
    }
}
