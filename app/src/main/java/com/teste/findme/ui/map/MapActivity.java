package com.teste.findme.ui.map;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.gms.maps.SupportMapFragment;
import com.teste.findme.BR;
import com.teste.findme.R;
import com.teste.findme.databinding.ActivityMapBinding;
import com.teste.findme.ui.base.BaseActivity;
import com.teste.findme.ui.report.ReportActivity;

public class MapActivity extends BaseActivity<ActivityMapBinding, MapViewModel> implements MapNavigation {


    private MapViewModel mMapViewModel;
    private ActivityMapBinding mActivityMapBinding;


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_map;
    }

    @Override
    public MapViewModel getViewModel() {
        mMapViewModel = ViewModelProviders.of(this).get(MapViewModel.class);
        return mMapViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivityMapBinding = getViewDataBinding();
        mMapViewModel.setNavigator(this);
        setToolbarTitle("Sua Localização");

        mMapViewModel.getCurrentLocation(this);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapViewModel.initMap(mapFragment);
    }

    @Override
    public void startReport() {
        mMapViewModel.startReport();
    }

    @Override
    public void openReportActivity() {
        Intent intent = new Intent(MapActivity.this, ReportActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void openHistoryActivity() {

    }
}
