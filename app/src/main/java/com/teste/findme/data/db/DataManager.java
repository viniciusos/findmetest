package com.teste.findme.data.db;

import com.teste.findme.data.model.Report;

import java.util.List;

import io.reactivex.Observable;

public interface DataManager extends DbHelper {

    void updateReport(
            String id,
            String lat,
            String lon,
            String content,
            String startAt,
            String endedAt);

}
