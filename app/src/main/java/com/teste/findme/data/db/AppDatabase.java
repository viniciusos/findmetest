package com.teste.findme.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.teste.findme.data.db.ReportDao;
import com.teste.findme.data.model.Report;

@Database(entities = {Report.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract ReportDao reportDao();
}
