package com.teste.findme.ui.report;

import com.teste.findme.data.db.DataManager;
import com.teste.findme.data.model.ReportForm;
import com.teste.findme.ui.base.BaseViewModel;

public class ReportViewModel extends BaseViewModel {

    ReportForm reportForm;

    public ReportForm getReport() {
        return reportForm;
    }

    public void onButtonClick() {
        reportForm.onClick();
    }

}
