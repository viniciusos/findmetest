package com.teste.findme.ui.history;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.teste.findme.R;
import com.teste.findme.data.model.Report;

import java.util.List;

import butterknife.BindView;

public class HistoryActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private HistoryAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private HistoryViewModel historyViewModel;
    ViewModelProvider factory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

    }

    public HistoryViewModel getViewModel() {
        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel.class);
        final Observer<List<Report>> observer = new Observer<List<Report>>() {
            @Override
            public void onChanged(@Nullable List<Report> reportList) {
                layoutManager = new LinearLayoutManager(HistoryActivity.this);
                ((LinearLayoutManager) layoutManager).setOrientation(LinearLayoutManager.VERTICAL);
                adapter = new HistoryAdapter(reportList);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(adapter);
            }
        };

        historyViewModel.getReportLiveData().observe(this, observer);
        historyViewModel.getReports();
        return historyViewModel;
    }
}
