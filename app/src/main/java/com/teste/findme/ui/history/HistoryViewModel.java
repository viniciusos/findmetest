package com.teste.findme.ui.history;

import android.arch.lifecycle.MutableLiveData;

import com.teste.findme.data.db.AppDbHelper;
import com.teste.findme.data.model.Report;
import com.teste.findme.ui.base.BaseViewModel;

import java.util.List;

public class HistoryViewModel extends BaseViewModel {

    private AppDbHelper reportRepository;
    private MutableLiveData<List<Report>> reportLiveData = new MutableLiveData();


    public void getReports() {
        reportRepository.loadAllReports();
    }

    public MutableLiveData<List<Report>> getReportLiveData() {
        return reportLiveData;
    }
}
