package com.teste.findme.data.db;

import android.content.Context;

import com.teste.findme.data.model.Report;

import java.util.List;

import io.reactivex.Observable;

public class AppDataManager implements DataManager {

    private DbHelper mDbHelper;

    public AppDataManager(DbHelper dbHelper) {
        mDbHelper = dbHelper;
    }

    @Override
    public void updateReport(String id, String lat, String lon, String content, String startAt, String endedAt) {

    }

    @Override
    public Observable<List<Report>> loadAllReports() {
        return mDbHelper.loadAllReports();

    }

    @Override
    public Observable<Boolean> insertReport(Report report) {
        return mDbHelper.insertReport(report);

    }
}
