package com.teste.findme.ui.history;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teste.findme.R;
import com.teste.findme.data.model.Report;
import com.teste.findme.utils.Core;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

    private List<Report> reportList;

    public HistoryAdapter(List<Report> reportList) {
        this.reportList = reportList;
    }


    public class HistoryViewHolder extends RecyclerView.ViewHolder {

        TextView latLon, startedAt, reportContent, endedAt;

        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);

            latLon = itemView.findViewById(R.id.lat_lon);
            startedAt = itemView.findViewById(R.id.started_at);
            reportContent = itemView.findViewById(R.id.report_content);
            endedAt = itemView.findViewById(R.id.ended_at);
        }

        private void bindView(Report report) {
            latLon.setText(report.getLatitude() + report.getLongitude()) ;
            startedAt.setText(Core.timestampToDate(report.getStartedAt()));
            reportContent.setText(report.getReportContent());
            endedAt.setText(Core.timestampToDate(report.getEndedAt()));
        }
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int view) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        HistoryViewHolder historyViewHolder = new HistoryViewHolder(itemView);
        return historyViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder historyViewHolder, int position) {
        historyViewHolder.bindView(reportList.get(position));
    }

    @Override
    public int getItemCount() {
        return reportList.size();
    }

}
