package com.teste.findme.data.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;


@Entity(tableName = "reports")
public class Report implements Parcelable {
    @PrimaryKey
    @ColumnInfo(name = "reportId")
    private String mId;

    @ColumnInfo(name = "latitude")
    private String latitude;

    @ColumnInfo(name = "longitude")
    private String longitude;

    @ColumnInfo(name = "startedAt")
    private long startedAt;

    @ColumnInfo(name = "reportContent")
    private String reportContent;

    @ColumnInfo(name = "endedAt")
    private long endedAt;

    public Report(){

    }
    protected Report(Parcel in) {
        mId = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        startedAt = in.readLong();
        reportContent = in.readString();
        endedAt = in.readLong();
    }

    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel in) {
            return new Report(in);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public long getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(long startedAt) {
        this.startedAt = startedAt;
    }

    public String getReportContent() {
        return reportContent;
    }

    public void setReportContent(String reportContent) {
        this.reportContent = reportContent;
    }

    public long getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(long endedAt) {
        this.endedAt = endedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeLong(startedAt);
        dest.writeString(reportContent);
        dest.writeLong(endedAt);
    }
}
