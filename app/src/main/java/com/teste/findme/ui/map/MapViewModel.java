package com.teste.findme.ui.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.teste.findme.data.db.AppDbHelper;
import com.teste.findme.data.db.DataManager;
import com.teste.findme.data.model.Report;
import com.teste.findme.ui.base.BaseViewModel;

public class MapViewModel extends BaseViewModel<MapNavigation> {

    private float ZOOM_MAP = 17.0f;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Report report;
    private LatLng latLng;

    private GoogleMap mMap;

    public void onClick() {
        getNavigator().startReport();
    }

    public void startReport() {

        getNavigator().openReportActivity();

    }

    public void initMap(final SupportMapFragment mapView) {

        if (mapView != null) {
            mapView.onCreate(new Bundle());

            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;
                    googleMap.getUiSettings().setAllGesturesEnabled(false);
                    googleMap.setMinZoomPreference(ZOOM_MAP);
                }
            });
        }
    }


    public void moveCamera(LatLng latLng) {
        this.latLng = latLng;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM_MAP));
        mMap.addMarker(new MarkerOptions().position(latLng).title("Seu local"));
    }

    public void getCurrentLocation(Context context) {
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        final Task location = mFusedLocationProviderClient.getLastLocation();
        location.addOnSuccessListener(new OnSuccessListener() {
            @Override
            public void onSuccess(@NonNull Object task) {
                Location currentLocation = (Location) task;
                if (currentLocation == null) {
                    return;
                }
                moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });

    }
}
