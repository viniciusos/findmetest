package com.teste.findme.ui.report;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.teste.findme.BR;
import com.teste.findme.R;
import com.teste.findme.data.model.Report;
import com.teste.findme.databinding.ActivityReportBinding;
import com.teste.findme.ui.base.BaseActivity;

public class ReportActivity extends BaseActivity<ActivityReportBinding, ReportViewModel> {
    private ReportViewModel mReportViewModel;
    private ActivityReportBinding mActivityReportBinding;

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_report;
    }

    @Override
    public ReportViewModel getViewModel() {
        mReportViewModel = ViewModelProviders.of(this).get(ReportViewModel.class);
        return mReportViewModel;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityReportBinding = getViewDataBinding();
    }
}
