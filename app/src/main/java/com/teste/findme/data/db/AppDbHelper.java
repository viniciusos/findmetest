package com.teste.findme.data.db;

import com.teste.findme.data.db.ReportDao;
import com.teste.findme.data.model.Report;

import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;

public class AppDbHelper implements DbHelper {

    private static ReportDao reportDao;
    private AppDatabase mAppDatabase;

    public AppDbHelper(AppDatabase appDatabase) {
        this.mAppDatabase = appDatabase;
    }

    @Override
    public Observable<List<Report>> loadAllReports() {
        return Observable.fromCallable(new Callable<List<Report>>() {
            @Override
            public List<Report> call() throws Exception {
                return mAppDatabase.reportDao().loadAll();
            }
        });
    }

    @Override
    public Observable<Boolean> insertReport(final Report report) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mAppDatabase.reportDao().insertReport(report);
                return true;
            }
        });
    }
}